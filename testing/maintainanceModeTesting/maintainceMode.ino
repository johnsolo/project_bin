void maintainceMode()
{
  int runUntilEnter = 1;

  while (runUntilEnter == 1 )
  {
    //................................. number ........................//
    if ( (userKey != 'E') && (userKey != 'C') && (userKey != 'D'))
    {
      if (userKeyCount < 5)
      {
        Serial.println(userKey);
        //phone number array
        maintaincePassArr[userKeyCount] = userKey;
        userKeyCount++;

        //LCD
        GlcdChar = userKey;//convert to string to display on LCD
        myGLCD.setFont(GroteskBold16x32);
        myGLCD.setColor(0, 0, 255);
        myGLCD.print(GlcdChar, shift, 170);
        shift += 16;

      }//if
    }//if number

    //................................. C - clear ........................//
    else if ( userKey == 'C' && userKeyCount >= 1)
    {
      //phone number array
      userKeyCount--;
      maintaincePassArr[userKeyCount] = userKey;

      //LDC
      shift -= 16;
      myGLCD.print(" ", shift, 170);
    }//if clear

    //................................. E - enter ........................//
    else if ( userKey == 'E' && userKeyCount == 5 )
    {
      //phone number send to GSM
      for ( int pp = 0; pp < 10; pp++ )
      {
        maintainceStr += maintaincePassArr[pp];
      }//for
      Serial.print("the entered password id: ");
      Serial.println(maintainceStr);
      //pass the string to GSM
      electronicLock();

      //initial state of the variables/string/arrays
      reset();

      //get out of while
      runUntilEnter = 0;

    }//if E

  }//while

}//void
