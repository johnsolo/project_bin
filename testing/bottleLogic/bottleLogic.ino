#include <Servo.h>

Servo door;
Servo lock;

int entrySensor = 43, sensor2 = 45, inverseSensor = 48, sensor3 = 49, CapSensor = 2, ThroSensor = 3, ReedSwitch = A1;//pin assignment for sensors
int  i, j, k, l, m, n, p;//varibles for stroing sensor values

int const trigPin = 20;
int const echoPin = 21;

void setup() {
  //
  Serial.begin(9600);
  delay(500);
  Serial.println("Welcome to SmartBin");

  //servo init
  door.attach(A5);
  lock.attach(A0);
  delay(1000);
  //initial angles
  door.write(0);
  lock.write(160);

  //sensor init
  pinMode(entrySensor, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(inverseSensor, INPUT);

  pinMode(CapSensor, INPUT);
  pinMode(ThroSensor, INPUT);

  pinMode(ReedSwitch, INPUT);
  pinMode(ReedSwitch, INPUT_PULLUP);

  pinMode(trigPin, OUTPUT); // trig pin will have pulses output
  pinMode(echoPin, INPUT); // echo pin should be input to get pulse width


}

void loop() {

  i = digitalRead(entrySensor);
  if ( i == 1 )
  {
    delay(1500);
    sensorStatus();

    //..................... large bottle .................//
    if ((k == 0 && l == 1 && m == 0 && n == 0) ||
        (k == 0 && l == 0 && m == 0 && n == 0) ||
        (k == 1 && l == 1 && m == 0 && n == 0))
    {
      //sensorStatusPrint();
      Serial.println("******large bottle*******");
      releaseBottle();
      delay(1000);
    }//if large bottle
    //..................... small bottle .................//
    else if (i == 1 && j == 0 && l == 0 && k == 1 && m == 0 && n == 0)
    {
      //sensorStatusPrint();
      Serial.println("******small bottle******");
      releaseBottle();
      delay(1000);
    }//if small bottle
    //..................... medium bottle .................//
    else if ((i == 1  || i == 0) && j == 1 && l == 0 && k == 1 && m == 0 &&  n == 0)
    {
      //sensorStatusPrint();
      Serial.println("*******medium bottle********");
      releaseBottle();
      delay(1000);
    }//if medium bottle
    //..................... other than bottle .................//
    else if (m == 1 || n == 1)
    {
      //sensorStatusPrint();
      Serial.println("********what is this!!*******");
      //releaseBottle();
      delay(1000);
    }//if other objects

  }//if entry detected


}//loop
















