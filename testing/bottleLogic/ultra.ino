
int ultra()
{
  unsigned int duration, distance;
  static int l;
  digitalWrite(trigPin, HIGH);
  delay(1);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;

  return distance;
  //Serial.print("ultra sensor: ");
  //Serial.println(distance);
}

/*
void ultra()
{
  float distance = 0, time = 0;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  time = pulseIn(echoPin, HIGH);
  distance = time * 340 / 20000;

  Serial.print("Sonar sensor: ");
  Serial.println(distance);
}
*/
