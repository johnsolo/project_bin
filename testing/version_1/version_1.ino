#include <SoftwareSerial.h>
#include <Servo.h>
#include <memorysaver.h>
#include <UTFT.h>
#include <Key.h>
#include <Keypad.h>

/***************************************************************************************************************** Servo *********************/
Servo door;
Servo lock;
/****************************************************************************************************************** Keypad ********************/
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'E'},
  {'D', '0', 'F', 'C'}
};
byte rowPins[ROWS] = {10, A12, 16, A11}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A10, A13, 6, A14}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);


/******************************************************************************************************************* Sensors*******************/
//pin assignment for sensors
int entrySensor = 43, sensor2 = 45, inverseSensor = 48, sensor3 = 49, CapSensor = 2, ThroSensor = 3, ReedSwitch = A1;
int  i, j, k, l, m, n, p; //varibles for stroing sensor values

int const trigPin = 20;
int const echoPin = 21;

/******************************************************************************************************************* Glcd *******************/
UTFT myGLCD(CTE32HR, 38, 39, 40, 41);//lcd init
extern uint8_t DotMatrix_M_Slash[];
extern uint8_t GroteskBold16x32[];
extern uint8_t SixteenSegment24x36[];
extern uint8_t BigFont[];
extern uint8_t SmallFont[];
extern unsigned short bin[];
extern unsigned short under_maintanence[];

/******************************************************************************************************************* variables for logic *******************/
int runOnce = 1; //to print the welcome message on Glcd

//variables for Keypad
char phoneNumber[11];
String numberToGsm;

char userKey;
int userKeyCount = 0;

//variables for Glcd
int shift = 128;
String GlcdChar;

//credits
int credit = 0;

/******************************************************************************************************************* GSM *******************/
String GsmNumber = "9902646070";


void setup() {
  //***************************************************** Servo *********//
  //servo init
  door.attach(A5);
  lock.attach(A0);
  delay(1000);
  //initial angles
  door.write(0);
  lock.write(160);

  //***************************************************** sensors *********//
  pinMode(entrySensor, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(inverseSensor, INPUT);

  pinMode(CapSensor, INPUT);
  pinMode(ThroSensor, INPUT);

  pinMode(ReedSwitch, INPUT);
  pinMode(ReedSwitch, INPUT_PULLUP);

  // trig pin will have pulses output
  pinMode(trigPin, OUTPUT);
  // echo pin should be input to get pulse width
  pinMode(echoPin, INPUT);

  //***************************************************** serial *********//
  //debug
  Serial.begin(9600);
  //gsm (18, 19)
  Serial1.begin(9600);

  //***************************************************** GLCD *********//
  //GLCD init
  myGLCD.InitLCD();

}

void loop() {
  //***********************************************************************************runonce *********//
  if ( runOnce == 1 )
  {
    myGLCD.clrScr();// clear screer
    delay(10);
    myGLCD.fillScr(255, 255, 255);
    myGLCD.drawBitmap(190, 30, 100, 100, bin);
    myGLCD.setFont(GroteskBold16x32);
    myGLCD.setBackColor(255, 255, 255);
    myGLCD.setColor(238, 118, 0);
    myGLCD.print("PLEASE PUT PLASTIC", CENTER, 160);
    myGLCD.print("BOTTLES ONLY", CENTER, 192);
    myGLCD.setFont(BigFont);
    myGLCD.print("MyPET", CENTER, 300);

    runOnce = 0;
  }//if runOnce

  //*********************************************************************************** bottle logic *********//
  i = digitalRead(entrySensor);
  if ( i == 1 )
  {
    delay(1500);
    sensorStatus();

    //.............................................................................. large bottle .................//
    if ((k == 0 && l == 1 && m == 0 && n == 0) ||
        (k == 0 && l == 0 && m == 0 && n == 0) ||
        (k == 1 && l == 1 && m == 0 && n == 0))
    {
      //sensorStatusPrint();
      Serial.println("******large bottle*******");
      credit = 750;
      largeBottle();
      delay(1000);
    }//if large bottle

    //.............................................................................. small bottle .................//
    else if (i == 1 && j == 0 && l == 0 && k == 1 && m == 0 && n == 0)
    {
      //sensorStatusPrint();
      Serial.println("******small bottle******");
      credit = 250;
      smallBottle();
      delay(1000);
    }//if small bottle
    
    //.............................................................................. medium bottle .................//
    else if ((i == 1  || i == 0) && j == 1 && l == 0 && k == 1 && m == 0 &&  n == 0)
    {
      //sensorStatusPrint();
      Serial.println("*******medium bottle********");
      credit = 500; 
      mediumBottle();     
      delay(1000);
    }//if medium bottle

  }//if entry detected
  
}//loop















