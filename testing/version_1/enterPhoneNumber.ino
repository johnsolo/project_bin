void enterPhoneNumber()
{
  int runUntilEnterPressed = 1;

  myGLCD.clrScr();
  myGLCD.setFont(GroteskBold16x32);
  myGLCD.setBackColor(0, 0, 0);
  myGLCD.setColor(255, 0, 0);
  myGLCD.print("Enter your mobile Number", CENTER, 50);
  myGLCD.print("and Press Enter", CENTER, 82);



  //************************************************************************************ user key press ***********************//
  while ( runUntilEnterPressed == 1 )
  {
    userKey = customKeypad.getKey();
    if ( userKey )
    {
      //................................. number ........................//
      if ( (userKey != 'E') && (userKey != 'C') )
      {
        if (userKeyCount < 10)
        {
          //phone number array
          phoneNumber[userKeyCount] = userKey;
          userKeyCount++;

          //LCD
          GlcdChar = userKey;//convert to string to display on LCD
          myGLCD.setFont(GroteskBold16x32);
          myGLCD.setColor(255, 255, 255);
          myGLCD.print(GlcdChar, shift, 170);
          shift += 16;

        }//if
      }//if number


      //................................. E - enter ........................//
      else if ( userKey == 'E' && userKeyCount == 10 )
      {
        //phone number send to GSM
        for ( int pp = 0; pp < 10; pp++ )
        {
          numberToGsm += phoneNumber[pp];
        }//for

        //pass the string to GSM
        sendMessage();

        //initial state of the variables/string/arrays
        reset();

        //get out of while
        runUntilEnterPressed = 0;

      }//if E
      //................................. C - clear ........................//
      else if ( userKey == 'C' && userKeyCount >= 1)
      {
        //phone number array
        userKeyCount--;
        phoneNumber[userKeyCount] = userKey;

        //LDC
        shift -= 16;
        myGLCD.print(" ", shift, 170);
      }//if clear

    }//if keypress

  }//while stuck


}//void

