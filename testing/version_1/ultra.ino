int ultra()
{
  unsigned int duration, distance;
  static int l;
  digitalWrite(trigPin, HIGH);
  delay(1);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;

  return distance;
  //Serial.print("ultra sensor: ");
  //Serial.println(distance);
}
