void reset()
{
  shift = 128;
  userKeyCount = 0;
  numberToGsm = "";
  memset(phoneNumber, 0, 11);
  
  runOnce = 1; //print the initial message back on LCD

  credit = 0;

  //print Thank you
  myGLCD.clrScr();
  myGLCD.setColor(0, 255, 0);
  myGLCD.setFont(GroteskBold16x32);
  myGLCD.print("Thank you", CENTER, 120);
  delay(3000);
}//void

