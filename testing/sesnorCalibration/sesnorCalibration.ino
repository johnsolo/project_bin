#include <Servo.h>

Servo door;
Servo lock;

int entrySensor = 43, sensor2 = 45, inverseSensor = 48, sensor3 = 49, CapSensor = 2, ThroSensor = 3, ReedSwitch = A1;//pin assignment for sensors
int  i, j, k, l, m, n, p;//varibles for stroing sensor values

int const trigPin = 20;
int const echoPin = 21;

void setup() {
  //
  Serial.begin(9600);
  delay(500);
  Serial.println("Welcome to SmartBin");

  //servo init
  door.attach(A5);
  lock.attach(A0);
  delay(1000);
  //initial angles
  door.write(0);
  lock.write(160);

  //sensor init
  pinMode(entrySensor, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(inverseSensor, INPUT);

  pinMode(CapSensor, INPUT);
  pinMode(ThroSensor, INPUT);

  pinMode(ReedSwitch, INPUT);
  pinMode(ReedSwitch, INPUT_PULLUP);
  
  pinMode(trigPin, OUTPUT); // trig pin will have pulses output
  pinMode(echoPin, INPUT); // echo pin should be input to get pulse width

  
}

void loop() {

  i = digitalRead(entrySensor);
  Serial.print("last/entry sensor: ");
  Serial.println(i);

  j = digitalRead(sensor2);
  Serial.print("second sensor: ");
  Serial.println(j);

  l = digitalRead(sensor3);
  Serial.print("third sensor: ");
  Serial.println(l);

  k = digitalRead(inverseSensor);
  Serial.print("inverse sensor: ");
  Serial.println(k);

  m = digitalRead(CapSensor);
  Serial.println("cap sensor: ");
  Serial.print(m);

  n = digitalRead(ThroSensor);
  Serial.print("through sensor: ");
  Serial.println(n);

  ultra();

  ReedSwitch=digitalRead(A1);
  Serial.print("Reed sensor: ");
  Serial.println(ReedSwitch);
  
  Serial.println(".............................................");
  delay(2000);
}//loop
