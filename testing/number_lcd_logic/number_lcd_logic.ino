#include <memorysaver.h>
#include <UTFT.h>

#include <Key.h>
#include <Keypad.h>

UTFT myGLCD(CTE32HR, 38, 39, 40, 41);//lcd init
extern uint8_t DotMatrix_M_Slash[];
extern uint8_t GroteskBold16x32[];
extern uint8_t SixteenSegment24x36[];
extern uint8_t BigFont[];
extern uint8_t SmallFont[];

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] =
{
  {'1', '2', '3', ' '},
  {'4', '5', '6', ' '},
  {'7', '8', '9', 'E'},
  {'D', '0', ' ', 'C'}
};
byte rowPins[ROWS] = {10, A12, 16, A11}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A10, A13, 6, A14}; //connect to the column pinouts of the keypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

int runOnce = 1;

char userKey;
int userKeyCount = 0;

int shift = 128;
String GlcdChar;

char phoneNumber[11];
String numberToGsm;

void setup() {
  //
  Serial.begin(9600);
  delay(100);

  //GLCD init
  myGLCD.InitLCD();
}

void loop() {
  //initial message on LCD
  if (runOnce == 1)
  {
    myGLCD.clrScr();
    myGLCD.setFont(GroteskBold16x32);
    myGLCD.setBackColor(0, 0, 0);
    myGLCD.setColor(255, 0, 0);
    myGLCD.print("Enter your mobile Number", CENTER, 50);
    myGLCD.print("and Press Enter", CENTER, 82);

    runOnce = 0;
  }//if

  //****************************** user key press ***********************//
  userKey = customKeypad.getKey();
  if ( userKey )
  {
    //................................. number ........................//
    if ( (userKey != 'E') && (userKey != 'C') )
    {
      if (userKeyCount < 10)
      {
        //phone number array
        phoneNumber[userKeyCount] = userKey;
        userKeyCount++;

        //LCD
        GlcdChar = userKey;//convert to string to display on LCD
        myGLCD.setFont(GroteskBold16x32);
        myGLCD.setColor(255, 255, 255);
        myGLCD.print(GlcdChar, shift, 170);
        shift += 16;

      }//if
    }//if number


    //................................. E - enter ........................//
    else if ( userKey == 'E' && userKeyCount == 10 )
    {
      //phone number send to GSM
      for ( int pp = 0; pp < 10; pp++ )
      {
        numberToGsm += phoneNumber[pp];
      }//for

      //pass the string to GSM
      sendMessage();

      //initial state of the variables/string/arrays
      reset();

    }//if E
    //................................. C - clear ........................//
    else if ( userKey == 'C' && userKeyCount >= 1)
    {
      //phone number array
      userKeyCount--;
      phoneNumber[userKeyCount] = userKey;

      //LDC
      shift -= 16;
      myGLCD.print(" ", shift, 170);
    }//if clear

  }//if keypress

}//loop








