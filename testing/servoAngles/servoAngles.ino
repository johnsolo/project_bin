#include <Servo.h>

Servo door;
Servo lock;

void setup() {
  // put your setup code here, to run once:
  door.attach(A5);
  lock.attach(A0);
  delay(1000);
  
  //initial angles
  door.write(0);
  lock.write(160);

  delay(3000);

  //runAngles
  door.write(160);
  lock.write(0);

}

void loop() {
  // put your main code here, to run repeatedly:

}//loop
