void bin_full()
{ /* getting reading from ultrasonic sensor to check bin is full or not*/
  int duration, distance = 20;
  static int l;
  digitalWrite(trigPin, HIGH);
  delay(1);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  //Serial.println(distance);
  if (distance != 0)
  {
    distance = distance - 20;
    percentage = distance;
  }
  else
  {
    percentage = "0";
  }
  //myGLCD.setColor(255, 69, 0);
  // myGLCD.setFont(BigFont);
  // myGLCD.print(  percentage+"%", RIGHT, 1);
  myGLCD.setFont(GroteskBold16x32);
  Serial.println(percentage);
  if (distance == 1000)
  {
    // object detected
    //bi=1;
    Serial.println(bi);
    myGLCD.setFont(GroteskBold16x32);
    myGLCD.setColor(255, 0, 0);
    myGLCD.setFont(SmallFont);
    myGLCD.print("BIN IS FULL", RIGHT, 0);
    myGLCD.setFont(GroteskBold16x32);
    //    disp=0;
    l++;


  }
  else
  {
    bi = 0;
    //myGLCD.setFont(SmallFont);
    // myGLCD.print(  percentage+"%", RIGHT, 0);
    //  myGLCD.setFont(GroteskBold16x32);
    if (l == 1)
    {
      disp = 0;
      l = 0;
      myGLCD.setFont(GroteskBold16x32);
    }
  }

  // Waiting 60 ms
  delay(60);
}
