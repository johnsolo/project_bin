
/*
 * created by Jhon
 * phone no: 9591564965
 * i->sensor 1 //entry IR sesnor
 * j->sensor2  //IR sensor
 * l->sensor3  //IR sensor
 * k->sensor4  //inverse-IR sensor
 * m->sensor6  //capacitive
 * n->sensor7  //through hole IR
 * servo // door motor
 * servo1//electronic lock motor
 * function description:
 * electronic lock()
 * num_func()->keypad for number mode
 * number_func1()->keypad for maintainence mode
 * send_()->GSM msg send;
 * ultrasonic()->how much full the bin is
 * reed_switch()->check status of the door
 * motor1()->opening and closing of channel door
 * motor2()->oops condition for channel door
 * check()->get values from sensor and print
 * varibles:
 * entry->store int entry of bottles
 * bi   ->used to switch if maintanence is being made or stop normal flow
 * disp ->used to set flag so that display will display only once
 * state->flag set when 10-digit phone number is received in keypad
 * count->flag for keypad
 * state2->flag for motor1
 * Reed_status->reed switch status
 * 
*/
#include <Keypad.h>
#include <Servo.h>
#include <UTFT.h>
#include <SoftwareSerial.h>
/*****************************************************LCD*********************************************************************************/
UTFT myGLCD(CTE32HR, 38, 39, 40, 41);//lcd init 
//SoftwareSerial Serial1(18,19);//serial init
Servo servo;//servo init
Servo servo1;
/***********************************************ELECTRONIC LOCK*******************************************************************************/
int Reed_Switch = A1;
int Reed_status = 0,read_status=0;
String msg="123";
/*************************************************NUM KEYPAD*******************************************************************************/
String b,aux1;           // used to store incoming number from keypad
char h;                  // used to switch to send mesage      
int shift = 128;         // shift is used to move the cursor position in display
static int credit;       // stores credit for bottle 
/***************************************************FONT******************************************************************************/
//font and bin image
extern uint8_t DotMatrix_M_Slash[];
extern uint8_t GroteskBold16x32[];
extern uint8_t SixteenSegment24x36[];
extern uint8_t BigFont[];
extern uint8_t SmallFont[];
extern unsigned short bin[];
extern unsigned short under_maintanence[];
/***********************************************SENSOR VARIBLES*****************************************************************************/
//Initilization of sensor varibles
int sensor1 = 43, sensor2 = 45, sensor3 = 48, sensor4 = 49, sensor5 = 2, sensor6 = 3;//pin assignment for sensors
int  i, j, k, l, m, n;//varibles for stroing sensor values
/*ultrasonic sensor pinout*/
int const trigPin = 20;
int const echoPin = 21;
int sensorValue = 0; //used to store ultrasonc sensor value
int sensorTriger = 0;
String percentage; //used to display how much % of bin is empty
/********************************************************VARIBLES****************************************************************************/
static int entry;//store int entry of bottles
static int bi=0; //used to switch if maintanence is being made
// Keypad varibles initialization
int num = 0; //counter for numbers entered
static int disp=0;  /* used to set flag so that display will display only once*/
char number[10], count = 0;// number is stored
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] =
{
  {'1', '2', '3', ' '},
  {'4', '5', '6', ' '},
  {'7', '8', '9', 'E'},
  {'D', '0', ' ', 'C'}
};
byte rowPins[ROWS] = {10, A12, 16, A11}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A10, A13, 6, A14}; //connect to the column pinouts of the keypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);
//Motor Varibles
//Servo motor;
//Stepper_28BYJ_48 stepper(10, 11, 12, 13);
//unsigned static int door_angle;
/****************************************************VARIBLES FOR ALS**********************************************************************************/
using namespace std;
const int analogInPin = A9;
const int trigerInPin = A8;
//int sensorValue = 0;  
//int sensorTriger = 1;
int arr[2000];
int dataSize = 0;
int Delay = 2000; //
int t = 0;
int alsflag = 0;
int timeOut = 0;
int subBit[8];
int a = 0;
int triger = 0;
int v = 0;
int als_flag;//als flag check
void setup()
{
  // put your setup code here, to run once:
  //servo  init
  servo.attach(A5);
  servo1.attach(A0);
  myGLCD.InitLCD();
  myGLCD.setFont(GroteskBold16x32);
  pinMode(sensor1, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(sensor4, INPUT);
  pinMode(sensor5, INPUT);
  pinMode(sensor6, INPUT);
  pinMode(trigPin, OUTPUT); // trig pin will have pulses output
  pinMode(echoPin, INPUT); // echo pin should be input to get pulse width
  pinMode(Reed_Switch, INPUT_PULLUP);
  pinMode(analogInPin, INPUT);
  pinMode(trigerInPin, INPUT);
  servo.write(0);
  servo1.write(180);
  Serial.begin(115200);
  Serial1.begin(9600);

}

// m and n are from capacitor and through sensor

void loop()
{
  
read_status=reed_switch();
  if (read_status==0)  
    { 
     
     delay(1000);
      servo1.write(180);
      
    bi=0;
  
    }// checks whether door is open if open close it 
  if(bi!= 1)
  {
  if(disp !=1)
  {
  myGLCD.clrScr();
  delay(10);
  myGLCD.clrScr();// clear screer
  myGLCD.fillScr(255, 255, 255);
  myGLCD.drawBitmap(190,30,100,100,bin);
  myGLCD.setFont(GroteskBold16x32);
 // myGLCD.setColor(255, 69, 0);//set colour for border
//  myGLCD.fillRect(0, 0 , 478, 318);
 // myGLCD.setColor(0, 0, 0);
 // myGLCD.fillRect(10, 10, 472, 312);
 // myGLCD.setColor(255, 255, 255);
  myGLCD.setColor(238, 118, 0);
  myGLCD.setBackColor(255, 255, 255);  
  myGLCD.print("PLEASE PUT PLASTIC", CENTER, 160);
  myGLCD.print("BOTTLES ONLY", CENTER, 192);
  myGLCD.setFont(BigFont); 
  myGLCD.print("MyPET", CENTER, 300);
 // myGLCD.setFont(SmallFont);
 // myGLCD.print(  percentage+"%", RIGHT, 0);
 bin_full();
 delay(100);
 myGLCD.setColor(255, 69, 0);
 myGLCD.setFont(BigFont);
 myGLCD.print(  percentage+"%", RIGHT, 1);
 myGLCD.setFont(GroteskBold16x32);
disp=1;
  } //prints the image in display only once
//  disp=1;
  
//  myGLCD.setColor(255, 69, 0);
// myGLCD.setFont(BigFont);
// myGLCD.print(  percentage+"%", RIGHT, 1);
//   myGLCD.setFont(GroteskBold16x32);

/*uncomment following line if u are going to use als*/
// als_flag=digitalRead(A8);
//      if(als_flag==1)
//    {     
//      als_maintainence();
//    }
number_Func1(); //function that allows you to open door for maintanence if als based system is not used
    
/* following logic will sense whether bottle is small medium or large*/       
  check();
  entry = digitalRead(sensor2);//sense the entry of bottle
  if (entry == 1)
  {myGLCD.setBackColor(0, 0, 0);
    delay(1500);
    check();
//
    if ((  k == 0 && l == 0 && m == 0 && n == 0) ||
        (  k == 1 && l == 1 && m == 0 && n == 0) ||
        (  k == 0 && l == 1 && m == 0 && n == 0))
    {
      Serial.println("Bottle is Large. Your credit is INR 30.");
      //door open close function call
       myGLCD.clrScr();
       myGLCD.setColor(255, 255, 0);
      myGLCD.print("LARGE", CENTER, 146); 
      delay(500);
      count=0;     
      credit=750;
      number_Func();//get number from user 
      motor1();//after the number is entered open or close door
      als_flag=0;
      disp=0;
    }
    else if (n == 1 && m == 1)
    {
      myGLCD.clrScr();
      myGLCD.setColor(255, 255, 0);
      myGLCD.print("TIN CAN", CENTER, 146);
      //Serial.println("Object is invalid.");
      //delay(2000);
      delay(500);
      //myGLCD.clrScr();
      motor1();
      disp=0;
    }
    else if ( i == 1 && j == 0 && k == 1 && l == 0 && m == 0 && n == 0 )
    {
      //Serial.println("Bottle is small. Your credit is INR 20.");
      myGLCD.clrScr();
      myGLCD.setColor(255, 255, 0);
      myGLCD.print("SMALL", CENTER, 146);
      //delay(2000);
      delay(500);
       als_flag=0;
      //myGLCD.clrScr();
      count=0; 
      credit=250;
      number_Func();
      motor1();        
      disp=0;    
    }
    else if ((( i == 1 || i == 0) && j == 1 && k == 1 && l == 0 && m == 0 && n == 0 ))
    {
      //Serial.println("Bottle is medium. Your credit is INR 10");
      myGLCD.clrScr();
      
      myGLCD.setColor(255, 255, 0);
      myGLCD.print("MEDIUM", CENTER, 146);
      delay(500);
       als_flag=0;
      //myGLCD.clrScr();
      count=0; 
      credit=500;
      number_Func();
      motor1();
      disp=0;

    }
    else if (i == 0 && j == 0 && k == 0 && l == 0 && m == 0 && n == 0 )
    {
      //Serial.println("Oops! We have sensed some issue.");
      myGLCD.clrScr();
      myGLCD.setColor(255, 255, 0);
      myGLCD.print("Oops!", CENTER, 146);
      //delay(2000);
      delay(500);
      motor2();
      disp=0;
    }
    else if(n==1)
    {
      myGLCD.clrScr();
      myGLCD.setColor(255, 0, 0);
      myGLCD.print("PAPER", CENTER, 146);
      //delay(2000);
      delay(500);
      motor1();
      disp=0; 
    }
      else if(m==1)
    {
      myGLCD.clrScr();
      myGLCD.setColor(255, 0, 0);
      myGLCD.print("GLASS", CENTER, 146);
      delay(500);
      //delay(2000);
      motor1();
      disp=0; 
    }
    else
    {/* if no bottle is detected but entry is shown*/
      //Serial.println("Oops2!");
      myGLCD.clrScr();
      myGLCD.setColor(255, 0, 0);
      myGLCD.print("Oops!", CENTER, 146);
      delay(500);
      //delay(2000);
      motor2();
      disp=0;
      }
    //delay(1000);
  }
       
}
}

void SendMessage()
{
  switch (h)
  {
    case 's': send_();
      break;
  }

}
